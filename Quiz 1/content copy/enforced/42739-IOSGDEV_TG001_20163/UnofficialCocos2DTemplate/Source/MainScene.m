#import "MainScene.h"
#import "Bullet.h"
#import "Enemy.h"

@interface MainScene()

@property (nonatomic, readwrite) float timeElapsed;
@property (nonatomic, readwrite) float timeElapsed2;
@property (nonatomic, readwrite) float lifetime;

@end

@implementation MainScene


-(instancetype)init
{
	if((self = [super init])){
        srand(time(NULL)); // Seed random number
        self.spawnInterval = 2.0f;
        self.userInteractionEnabled = YES;
        
        _player = [Player playerWithImageNamed:@"playerShip1_blue.png" andSpeed:200];
        _player.sprite.position = ccp(300,50);
        [self addChild:_player.sprite z:1];
	}
	
	return self;
}

- (void)update:(CCTime)delta
{
    [_player update:delta];
    
    _lifetime += delta;
    _timeElapsed += delta;
    if (_timeElapsed >= self.spawnInterval) {
        int randomStrafe;
        int randomCheck = (rand() % 100);
        
        // If rolled number is greater than 50, then strafe left
        if ((randomCheck) > 50)
        {
            randomStrafe = -1;
        }
        
        // If rolled number is less than 51, then strafe right
        else
        {
            randomStrafe = 1;
        }
        
        // Spawn enemy
        Enemy* enemy = [Enemy enemyWithImageNamed:@"playerShip2_red.png" andSpeed:70.0f andDirection:ccp(randomStrafe,-5)];
        
        [self addChild:enemy];
        
        // Random position
        CGSize size = [[CCDirector sharedDirector] viewSize];
        int x = rand() % (int)size.width;
        
        [enemy setPosition:ccp(x,size.height)];
        
        _timeElapsed = 0.0f;
        NSLog(@"Before = %f", _lifetime);
        
        // Enemy disappears after 5 seconds
        if (_lifetime > 5)
        {
            [self removeChild:enemy];
            _lifetime = 0.0f;
        }
    }
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [_player moveTo:touch.locationInWorld];
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [_player moveTo:touch.locationInWorld];
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [_player moveTo:touch.locationInWorld];
}












@end
