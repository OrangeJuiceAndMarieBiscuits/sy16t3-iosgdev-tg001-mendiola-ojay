//
//  Bullet.h
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 9/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#ifndef Bullet_h
#define Bullet_h

#import <Foundation/Foundation.h>

@interface Bullet : CCSprite

+(id)bulletWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction;

-(id)initWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction;

@property (nonatomic, assign) CGPoint direction;
@property (nonatomic, assign) float speed;

@end


#endif /* Bullet_h */
