#import "Player.h"

@interface MainScene : CCScene

@property (nonatomic, assign) float spawnInterval;
@property (nonatomic, strong) Player *player;
@end
