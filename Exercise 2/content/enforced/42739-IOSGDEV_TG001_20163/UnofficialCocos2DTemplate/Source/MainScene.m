#import "MainScene.h"

@interface MainScene()
@property (nonatomic, strong) CCNode *uiLayer;
@property (nonatomic, strong) CCNode *gameLayer;
@end

@implementation MainScene

-(instancetype)init
{
    if((self = [super init])){
        srand(time(NULL)); // Seed random number
        self.userInteractionEnabled = true;
        self.multipleTouchEnabled = true;
        
        // It's recommended to have a separate UILayer so that the HUD does not move along w/ the game scene
        _uiLayer = [CCNode node];
        [self addChild:_uiLayer];
        
        _gameLayer = [CCNode node];
        [self addChild:_gameLayer];
        
        // How to declare a label
        CCLabelTTF* label = [CCLabelTTF labelWithString:@"Weab1 goes to jail" fontName:@"Consolas" fontSize:20];
        [label setColor:[CCColor redColor]];
        
        // Can also position labels the same way sprites are positioned
        label.position = ccp(200, 200);
        
        // Add label to uiLayer
        [_uiLayer addChild:label];
        
        // How to declare buttons
        CCButton* button = [CCButton buttonWithTitle:nil spriteFrame:[CCSpriteFrame frameWithImageNamed:@"fireButton.png"]];
        
        // Can also position buttons the same way sprites are positioned
        button.position = ccp(200, 200);
        // To get a callback when a button is touched, setTarget
        // The first parameter is the instance where to call the function
        // The second parameter is what function to call
        [button setTarget:self selector:@selector(onButtonClick)];
        [_uiLayer addChild:button];
    }
    
    return self;
}

- (void)onButtonClick
{
    NSLog(@"Button clicked");
}
@end








