#import "MainScene.h"

@interface MainScene()
@property (nonatomic, readwrite) float timeElasped;
@property (nonatomic, strong) CCPhysicsNode *physicsWorld;
@property (nonatomic, strong) Player *player;
@property (nonatomic, strong) Asteroid *asteroid;
@end

@implementation MainScene
Asteroid *asteroid;

-(instancetype)init
{
	if((self = [super init])){
        srand(time(NULL)); // Seed random number
        self.spawnInterval = 2.0f;
        self.userInteractionEnabled = true;
        self.multipleTouchEnabled = true;
        
        _player = [Player playerWithImageNamed:@"playerShip1_blue.png" andSpeed:100.0f];
        _player.sprite.position = ccp(100, 300);
        
        _asteroid = [Asteroid asteroidWithImageNamed:@"meteorBrown_big1.png" andSpeed:0.0f andDirection:CGPointZero];
        _asteroid.position = ccp(100, 100);
        
        // Setup physics world
        _physicsWorld = [CCPhysicsNode node];
        _physicsWorld.gravity = ccp(0,0);
        _physicsWorld.collisionDelegate = self;
        [self addChild:_physicsWorld];
        
        // Setup player physics
        // Creates a physics body, in this case a box using the player’s contentSize to create an bounding box rectangle around the player.
        CGRect playerRect;
        playerRect.origin = CGPointZero;
        playerRect.size = _player.sprite.contentSize;
        _player.sprite.physicsBody = [CCPhysicsBody bodyWithRect:playerRect cornerRadius:0];
        // Set physics body collisionGroup, by default everything will collide.
        // If you set physics bodies to the same collisionGroup they will no longer collide with each other,
        // this is handy when you have a player that is made up of multiple bodies
        // but you don’t want these bodies to collide with each other,
        // for example a player holding a weapon. You will use this to ensure the projectile
        // does not collide with the player.
        _player.sprite.physicsBody.collisionGroup = @"playerGroup";
        _player.sprite.physicsBody.collisionType = @"playerCollision";
        
        CGRect asteroidRect;
        asteroidRect.origin = CGPointZero;
        asteroidRect.size = _asteroid.contentSize;
        _asteroid.physicsBody = [CCPhysicsBody bodyWithRect:asteroidRect cornerRadius:0];
        _asteroid.physicsBody.collisionGroup = @"asteroidGroup";
        _asteroid.physicsBody.collisionType = @"asteroidCollisiion";
        
        [_physicsWorld addChild:_player.sprite];
        [_physicsWorld addChild:_asteroid];
	}
	
	return self;
}

// Collision callback
// The proper signature is
//- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair typeA:(CCNode *)nodeA typeB:(CCNode *)nodeB
// But you need to replace "typeA" and "typeB"
// With the collision types you set earlier
// In this example, it's playerCollision and asteroidCollision
- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)nodeA asteroidCollisiion:(CCNode *)nodeB
{
    NSLog(@"Collision");
    return YES;
}

- (void)update:(CCTime)delta
{
    [_player update:delta];
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [_player moveTo:touch.locationInWorld];
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [_player moveTo:touch.locationInWorld];
}

@end