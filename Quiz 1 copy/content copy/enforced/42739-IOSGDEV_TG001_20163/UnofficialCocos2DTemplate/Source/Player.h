//
//  Player.h
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 10/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#pragma once
#import <Foundation/Foundation.h>

@interface Player : NSObject

@property (nonatomic, strong) CCSprite *sprite;
@property (nonatomic, readwrite) float speed;

+(id)playerWithImageNamed:(NSString*)name andSpeed:(float)speed;

-(id)initWithImageNamed:(NSString*)name andSpeed:(float)speed;

-(void)update:(float)deltaTime;

-(void)moveTo:(CGPoint)location;
-(void)stopMovement;
@end










