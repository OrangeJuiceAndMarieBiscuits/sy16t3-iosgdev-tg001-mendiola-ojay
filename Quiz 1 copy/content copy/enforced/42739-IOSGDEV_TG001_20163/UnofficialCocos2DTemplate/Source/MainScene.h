#import "Asteroid.h"
#import "Player.h"

@interface MainScene : CCScene<CCPhysicsCollisionDelegate>

@property (nonatomic, assign) float spawnInterval;

@end
