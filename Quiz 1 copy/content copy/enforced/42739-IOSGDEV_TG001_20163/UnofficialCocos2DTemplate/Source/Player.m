//
//  Player.m
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 10/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "Player.h"

@interface Player ()
@property (nonatomic, readwrite) CGPoint targetLocation;
@property (nonatomic, readwrite) BOOL isMoving;
@end

@implementation Player

+(id)playerWithImageNamed:(NSString *)name andSpeed:(float)speed
{
    return [[self alloc] initWithImageNamed:name andSpeed:speed];
}

-(id)initWithImageNamed:(NSString *)name andSpeed:(float)speed
{
    self = [super init];
    if (self)
    {
        _sprite = [CCSprite spriteWithImageNamed:name];
        _speed = speed;
    }
    return self;
}

- (void)update:(float)deltaTime
{
    if (!_isMoving) return;
    
    float distance = ccpDistance(_targetLocation, _sprite.position);
    
    if (distance < 1.0f) {
        _isMoving = NO;
        return;
    }
    
    CGPoint dir = ccpSub(_targetLocation, _sprite.position);
    dir = ccpNormalize(dir);
    dir = ccpMult(dir, deltaTime);
    dir = ccpMult(dir, _speed);
    
    _sprite.position = ccpAdd(_sprite.position, dir);
}

- (void)moveTo:(CGPoint)location
{
    _targetLocation = location;
    _isMoving = YES;
}

- (void)stopMovement
{
    _isMoving = NO;
}






@end