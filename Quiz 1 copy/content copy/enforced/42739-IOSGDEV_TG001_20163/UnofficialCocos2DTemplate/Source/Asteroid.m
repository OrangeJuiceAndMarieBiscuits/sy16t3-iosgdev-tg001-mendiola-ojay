//
//  Asteroid.m
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 9/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Asteroid.h"
@implementation Asteroid

+(id)asteroidWithImageNamed:(NSString *)fileName andSpeed:(float)speed andDirection:(CGPoint)direction
{
    return [[self alloc] initWithImageNamed:fileName andSpeed:speed andDirection:direction];
}

-(id)initWithImageNamed:(NSString *)fileName andSpeed:(float)speed andDirection:(CGPoint)direction{
    self = [super initWithImageNamed:fileName];
    if (self) {
        self.speed = speed;
    }
    return self;
}

@end

