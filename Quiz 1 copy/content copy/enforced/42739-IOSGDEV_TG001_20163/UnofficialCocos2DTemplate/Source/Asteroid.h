//
//  Asteroid.h
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 9/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#ifndef Asteroid_h
#define Asteroid_h

#import <Foundation/Foundation.h>

@interface Asteroid : CCSprite

+(id)asteroidWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction;

-(id)initWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction;

@property (nonatomic, assign) float speed;

@end


#endif /* Asteroid_h */
