//
//  Background.h
//  UnofficialCocos2DTemplate.git
//
//  Created by Ojay Mendiola on 8/4/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#ifndef Background_h
#define Background_h

#import <Foundation/Foundation.h>

@interface Background: CCSprite

+(id)backgroundWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration;

-(id)initWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration;

@property (nonatomic, assign) CGPoint direction;
@property (nonatomic, assign) float speed;
@property (nonatomic, assign) float duration;

@end


#endif /* Background_h */
