//
//  Stars.h
//  UnofficialCocos2DTemplate.git
//
//  Created by Ojay Mendiola on 8/3/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#ifndef Stars_h
#define Stars_h

#import <Foundation/Foundation.h>

@interface Stars : CCSprite

+(id)starsWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration;

-(id)initWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration;

@property (nonatomic, assign) CGPoint direction;
@property (nonatomic, assign) float speed;
@property (nonatomic, assign) float duration;

@end


#endif /* Stars_h */
