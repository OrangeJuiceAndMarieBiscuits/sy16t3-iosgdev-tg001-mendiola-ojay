//
//  Enemy.h
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 9/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#ifndef Enemy_h
#define Enemy_h

#import <Foundation/Foundation.h>

@interface Enemy : CCSprite

+(id)enemyWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration;

-(id)initWithImageNamed:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration;

@property (nonatomic, assign) CGPoint direction;
@property (nonatomic, assign) float speed;
@property (nonatomic, assign) float duration;

@end


#endif /* Enemy_h */
