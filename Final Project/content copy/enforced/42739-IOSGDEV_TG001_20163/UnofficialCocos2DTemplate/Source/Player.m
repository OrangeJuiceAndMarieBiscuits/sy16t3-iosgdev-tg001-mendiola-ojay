//
//  Player.m
//  UnofficialCocos2DTemplate.git
//
//  Created by Kevin Brian Valmonte (Instructor) on 10/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "Player.h"

@interface Player ()
@property (nonatomic, readwrite) CGPoint targetLocation;
@property (nonatomic, readwrite) BOOL isMoving;
@property (nonatomic, readwrite) BOOL currentlyMoving;
@end

@implementation Player

+(id)playerWithImageNamed:(NSString *)name andSpeed:(float)speed
{
    return [[self alloc] initWithImageNamed:name andSpeed:speed];
}

-(id)initWithImageNamed:(NSString *)name andSpeed:(float)speed
{
    self = [super init];
    if (self)
    {
        _sprite = [CCSprite spriteWithImageNamed:name];
        _speed = speed;
    }
    return self;
}

- (void)update:(float)deltaTime
{
    if (!_isMoving) return;
    
    CGSize size = [[CCDirector sharedDirector] viewSize];
    
    // Move left
    if(_targetLocation.x < (size.width/2) && _currentlyMoving == NO)
    {
        _sprite.position = ccpAdd(_sprite.position, ccp(-4,0));
    }
    
    // Move right
    if(_targetLocation.x > (size.width/2) && _currentlyMoving == NO)
    {
        _sprite.position = ccpAdd(_sprite.position, ccp(4,0));
    }
    
    // Move to top side
    if(_targetLocation.y > (size.height/2))
    {
        _sprite.position = ccpAdd(_sprite.position, ccp(0,6));
        _currentlyMoving = YES;
    }
    
    // Move to bottom side
    if(_targetLocation.y < (size.height/2))
    {
        _sprite.position = ccpAdd(_sprite.position, ccp(0,-6));
        _currentlyMoving = YES;
    }
    
    // Clamp left
    if (_sprite.position.x < (size.width*0.1))
    {
        _sprite.position = ccp((size.width*0.1),_sprite.position.y);
    }
    
    // Clamp right
    if (_sprite.position.x > (size.width*0.9))
    {
        _sprite.position = ccp((size.width*0.9),_sprite.position.y);
    }
    
    // Clamp top
    if (_sprite.position.y > (size.height*0.8))
    {
        _sprite.position = ccp(_sprite.position.x,(size.height*0.8));
        _currentlyMoving = NO;
    }
    
    // Clamp bottom
    if (_sprite.position.y < (size.height*0.2))
    {
        _sprite.position = ccp(_sprite.position.x,(size.height*0.2));
        _currentlyMoving = NO;
    }
}

- (void)moveTo:(CGPoint)location
{
    _targetLocation = location;
    _isMoving = YES;
}

- (void)stopMovement
{
    _isMoving = NO;
}

@end
