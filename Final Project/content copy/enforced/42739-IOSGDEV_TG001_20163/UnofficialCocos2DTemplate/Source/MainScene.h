#import "Player.h"
#import "Stars.h"
#import "Enemy.h"

@interface MainScene : CCScene<CCPhysicsCollisionDelegate>

@property (nonatomic, assign) float spawnInterval;
@property (nonatomic, strong) Player *player;
@end
