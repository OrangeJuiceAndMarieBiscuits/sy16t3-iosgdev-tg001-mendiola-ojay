//
//  Stars.m
//  UnofficialCocos2DTemplate.git
//
//  Created by Ojay Mendiola on 8/3/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stars.h"
@implementation Stars

+(id)starsWithImageNamed:(NSString *)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration
{
    return [[self alloc] initWithImageNamed:fileName andSpeed:speed andDirection:direction andDuration:duration];
}

-(id)initWithImageNamed:(NSString *)fileName andSpeed:(float)speed andDirection:(CGPoint)direction andDuration:(float)duration{
    self = [super initWithImageNamed:fileName];
    if (self) {
        self.direction = direction;
        self.speed = speed;
        self.duration = duration;
    }
    return self;
}

-(void)update:(CCTime)delta
{
    float deltaSpeed = [self speed] * delta * 1;
    self.direction = ccpNormalize([self direction]);
    CGPoint translationVector = ccp(self.direction.x * deltaSpeed, self.direction.y * deltaSpeed);
    
    [self setPosition:ccp(self.position.x + translationVector.x, self.position.y + translationVector.y)];
    [self setDuration:_duration];
}

@end

