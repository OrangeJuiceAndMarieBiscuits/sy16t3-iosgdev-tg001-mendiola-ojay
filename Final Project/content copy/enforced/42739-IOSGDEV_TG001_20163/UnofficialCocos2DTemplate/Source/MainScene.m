#import "MainScene.h"
#import "Enemy.h"
#import "Stars.h"
#import "Background.h"

@interface MainScene()

@property (nonatomic, strong) CCNode *uiLayer;
@property (nonatomic, strong) CCNode *gameLayer;
@property (nonatomic, strong) CCPhysicsNode *physicsWorld;
@property (nonatomic, readwrite) int score;
@property (nonatomic, readwrite) int scoreBonus;
@property (nonatomic, readwrite) int invulnerabilityCount;
@property (nonatomic, readwrite) bool invulnerable;
@property (nonatomic, readwrite) float invulnerabilityCurrentDuration;
@property (nonatomic, readwrite) float invulnerabilityChosenDuration;
@property (nonatomic, readwrite) float timeElapsed;
@property (nonatomic, readwrite) float lifetime;
@property (nonatomic, readwrite) float enemySpeedIncrease;
@property (nonatomic, readwrite) CCLabelTTF* label;
@property (nonatomic, readwrite) CCLabelTTF* label2;
@property (nonatomic, readwrite) CCLabelTTF* labelInvulCount;
@property (nonatomic, readwrite) CCLabelTTF* labelInvulDuration;
@property (nonatomic, readwrite) CGSize size;
@end

@implementation MainScene
Player* player;

-(instancetype)init
{
	if((self = [super init])){
        srand(time(NULL)); // Seed random number
        self.spawnInterval = 3.0f;
        self.userInteractionEnabled = true;
        self.multipleTouchEnabled = true;
        
        _size = [[CCDirector sharedDirector] viewSize];
        
        _enemySpeedIncrease = 0;
        _invulnerable = NO;
        _invulnerabilityCount = 0;
        _invulnerabilityCurrentDuration = 0.0f;
        _invulnerabilityChosenDuration = 5.0f;
        
        // Sets up background
        Background *background = [Background backgroundWithImageNamed:@"Background.png" andSpeed:0.0f andDirection:ccp(1,0) andDuration:1.0f];
        [background setPosition:ccp((_size.width/2),(_size.height/2))];
        [background setDuration:1.0f];
        [background setScale:1.2f];
        [self addChild:background];
        
        // Sets up top bar
        Background *topBar = [Background backgroundWithImageNamed:@"Bar.png" andSpeed:0.0f andDirection:ccp(1,0) andDuration:1.0f];
        [topBar setPosition:ccp((_size.width/2),(_size.height*0.94))];
        [topBar setDuration:1.0f];
        [topBar setScaleX:1.025f];
        [self addChild:topBar];
        
        // Sets up bottom bar
        Background *bottomBar = [Background backgroundWithImageNamed:@"Bar.png" andSpeed:0.0f andDirection:ccp(1,0) andDuration:1.0f];
        [bottomBar setPosition:ccp((_size.width/2),(_size.height*0.055))];
        [bottomBar setDuration:1.0f];
        [bottomBar setScaleX:1.025f];
        [self addChild:bottomBar];
        
        player = [Player playerWithImageNamed:@"Player.png" andSpeed:1];
        player.sprite.position = ccp((_size.width/2),(_size.height*0.2));
        
        // Setup physics world
        _physicsWorld = [CCPhysicsNode node];
        _physicsWorld.gravity = ccp(0,0);
        _physicsWorld.collisionDelegate = self;
        [self addChild:_physicsWorld];
        
        // Setup player physics
        CGRect playerRect;
        playerRect.origin = CGPointZero;
        playerRect.size = player.sprite.contentSize;
        player.sprite.physicsBody = [CCPhysicsBody bodyWithRect:playerRect cornerRadius:0];
        player.sprite.physicsBody.collisionGroup = @"playerGroup";
        player.sprite.physicsBody.collisionType = @"playerCollision";
        player.sprite.scale = 0.8f;
        [_physicsWorld addChild:player.sprite];
        
        // UI
        _uiLayer = [CCNode node];
        [self addChild:_uiLayer];
        
        _gameLayer = [CCNode node];
        [self addChild:_gameLayer];
        
        _label = [CCLabelTTF labelWithString:@"Score: " fontName:@"Consolas" fontSize:16];
        [_label setColor:[CCColor blackColor]];
        _labelInvulCount = [CCLabelTTF labelWithString:@"Invulnerability Count: " fontName:@"Consolas" fontSize:16];
        [_labelInvulCount setColor:[CCColor blackColor]];
        
        _label.position = ccp(_size.width*0.13, _size.height*0.35);
        _labelInvulCount.position = ccp(_size.width*0.217, _size.height*0.275);
        
        [_uiLayer addChild:_label];
        [_uiLayer addChild:_labelInvulCount];
	}
	
	return self;
}

- (void)update:(CCTime)delta
{
    [player update:delta];
    
    _label.string = [NSString stringWithFormat:@"Score: %i", _score ];
    _labelInvulCount.string = [NSString stringWithFormat:@"Invulnerability Count: %i/5", _invulnerabilityCount ];
    _labelInvulDuration.string = [NSString stringWithFormat:@"Invulnerability Duration: %f", (5-_invulnerabilityCurrentDuration)];
    _timeElapsed += delta;
    
    // Spawns an enemy and a star every 3 seconds, and adds +30 speed to the next enemy
    if (_timeElapsed >= self.spawnInterval)
    {
        [self spawnStar];
        [self spawnEnemy];
        _enemySpeedIncrease += 30;
        _spawnInterval -= 0.2f;
        NSLog(@"%f", _spawnInterval);
        NSLog(@"%f", _enemySpeedIncrease);
        if(_spawnInterval < 1.0f)
        {
            _enemySpeedIncrease = 70.0f;
            _spawnInterval = 3.0f;
        }
    }
    
    // Counts down invulnerability
    if (_invulnerable == YES)
    {
        _invulnerabilityCurrentDuration += delta;

        if (_invulnerabilityCurrentDuration >= _invulnerabilityChosenDuration)
        {
            // Stop invulnerability when timer goes 0
            [_uiLayer removeChild:_labelInvulDuration];
            _invulnerable = NO;
            _invulnerabilityCurrentDuration = 0;
            _invulnerabilityCount = 0;
        }
    }
}

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)nodeA starCollisiion:(CCNode *)nodeB
{
    // Score
    // Scoring basically starts at 5, for every collected star the addend is increased by 5
    // Eg. 5, 15, 30, 50, 75, 105, and so on.....
    _score += 5 + _scoreBonus;
    _scoreBonus += 5;
    
    // Invulnerability Determiner
    if(_invulnerabilityCount < 4)
    {
        _invulnerabilityCount += 1;
    }
    else if(_invulnerabilityCount == 4)
    {
        _invulnerabilityCount += 1;
        
        _invulnerable = YES;
        _labelInvulDuration = [CCLabelTTF labelWithString:@"Score: " fontName:@"Comic Sans Serif" fontSize:16];
        [_labelInvulDuration setColor:[CCColor blackColor]];
        _labelInvulDuration.position = ccp(_size.width*0.271, _size.height*0.2);
        [_uiLayer addChild:_labelInvulDuration];
    }
    
    [_physicsWorld removeChild:nodeB];
    return YES;
}

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)nodeA enemyCollisiion:(CCNode *)nodeB
{
    // Player Death
    if(_invulnerable == NO)
    {
        [_physicsWorld removeChild:nodeA];
        
        // Game Over label
        _label2 = [CCLabelTTF labelWithString:@"Game Over" fontName:@"Consolas" fontSize:30];
        [_label2 setColor:[CCColor redColor]];
        _label2.position = ccp(_size.width/2, _size.height/2);
        [_uiLayer addChild:_label2];
        [_uiLayer removeChild:_labelInvulCount];
        [_uiLayer removeChild:_label];
        return YES;
    }
}

- (void) spawnEnemy
{
    // Random position (either left or right)
    int direction;
    int x = rand() % (int)_size.width;
    if (x >= (_size.width/2))
    {
        x = _size.width*0.9;
        direction = -1;
    }
    else if (x < (_size.width/2))
    {
        x = _size.width*0.1;
        direction = 1;
    }
    
    // Spawn enemy
    Enemy* enemy = [Enemy enemyWithImageNamed:@"Enemy.png" andSpeed:70.0f andDirection:ccp(direction,0) andDuration:1.0f];
    
    CGRect enemyRect;
    enemyRect.origin = CGPointZero;
    enemyRect.size = enemy.contentSize;
    enemy.physicsBody = [CCPhysicsBody bodyWithRect:enemyRect cornerRadius:0];
    enemy.physicsBody.collisionGroup = @"enemyGroup";
    enemy.physicsBody.collisionType = @"enemyCollisiion";

    [enemy setPosition:ccp(x,(_size.height/2))];
    [enemy setDuration:1.0f];
    [enemy setScale:0.7f];
    [enemy setSpeed:enemy.speed + _enemySpeedIncrease];
    
    [_physicsWorld addChild:enemy];

    _timeElapsed = 0.0f;
}

- (void) spawnStar
{
    // Random position
    int x = rand() % (int)_size.width;
    if (x >= (_size.width/2))
    {
        x = _size.width*0.8;
    }
    else if (x < (_size.width/2))
    {
        x = _size.width*0.2;
    }
    
    int y = rand() % (int)_size.height;
    if (y >= (_size.height/2))
    {
        y = _size.height*0.8;
    }
    else if (x < (_size.height/2))
    {
        y = _size.height*0.2;
    }
    
    // Spawn star
    Stars* star = [Stars starsWithImageNamed:@"Battery.png" andSpeed:0.0f andDirection:ccp(1,0) andDuration:1.0f];
    
    CGRect starRect;
    starRect.origin = CGPointZero;
    starRect.size = star.contentSize;
    star.physicsBody = [CCPhysicsBody bodyWithRect:starRect cornerRadius:0];
    star.physicsBody.collisionGroup = @"enemyGroup";
    star.physicsBody.collisionType = @"starCollisiion";
    
    [star setPosition:ccp(x,y)];
    [star setDuration:1.0f];
    [star setScale:0.7f];
    
    [_physicsWorld addChild:star];
    
    _timeElapsed = 0.0f;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [player moveTo:touch.locationInWorld];
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [player moveTo:touch.locationInWorld];
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    //[player moveTo:touch.locationInWorld];
}
@end
