//
//  Wizard.h
//  Exercise 1 Mendiola O
//
//  Created by Ojay Mendiola on 5/23/17.
//  Copyright © 2017 Ojay Mendiola. All rights reserved.
//

#ifndef Wizard_h
#define Wizard_h

#import <Foundation/Foundation.h>

@interface Wizard : NSObject

// Public properties
@property (nonatomic, readwrite) int hp;
@property (nonatomic, readwrite) int mp;
@property (nonatomic, strong) NSString *name;


// Static constructor. USE THIS
+(id)wizardWithName:(NSString*)name andHp:(int)hp andMp:(int)mp;
// Constructor
-(id)wizardWithName:(NSString*)name andHp:(int)hp andMp:(int)mp;

// Public functions
-(void)useSpell;

@end

#endif /* Wizard_h */
