//
//  Spell.h
//  Exercise 1 Mendiola O
//
//  Created by Ojay Mendiola on 5/23/17.
//  Copyright © 2017 Ojay Mendiola. All rights reserved.
//

#ifndef Spell_h
#define Spell_h

#import <Foundation/Foundation.h>

@interface Spell : NSObject

// Public properties
@property (nonatomic, readwrite) int cost;
@property (nonatomic, readwrite) int damage;
@property (nonatomic, strong) NSString *name;


// Static constructor. USE THIS
+(id)spellWithName:(NSString*)name andCost:(int)cost andDamage:(int)damage;
// Constructor
-(id)spellWithName:(NSString*)name andCost:(int)cost andDamage:(int)damage;

// Public functions
-(void)use;

@end

#endif /* Item_h */
