//
//  Spell.m
//  Exercise 1 Mendiola O
//
//  Created by Ojay Mendiola on 5/23/17.
//  Copyright © 2017 Ojay Mendiola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Spell.h"

@interface Spell ()
// Private variables
@property (nonatomic, readwrite) int useCount;
@end

@implementation Spell

+ (id)spellWithName:(NSString *)name andCost:(int)cost andDamage:(int)damage
{
    // Always return [[self alloc] init.....]
    return [[self alloc] initWithName:name andCost:cost andDamage:(int)damage];
}

- (id)initWithName:(NSString *)name andCost :(int)cost andDamage:(int)damage
{
    @autoreleasepool {
        self = [super init];
        if (self)
        {
            _name = name;
            _cost = cost;
            _damage = damage;
        }
        return self;
    }
}

//- (void)use
//{
//    NSLog(@"Spell used!");
//    _useCount++;
//}

@end
