//
//  Wizard.m
//  Exercise 1 Mendiola O
//
//  Created by Ojay Mendiola on 5/23/17.
//  Copyright © 2017 Ojay Mendiola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Wizard.h"

@interface Wizard ()
// Private variables
@property (nonatomic, readwrite) int useCount;
@end

@implementation Wizard

+ (id)wizardWithName:(NSString *)name andHp:(int)hp andMp:(int)mp
{
    // Always return [[self alloc] init.....]
    return [[self alloc] initWithName:name andHp:(int)hp andMp:(int)mp];
}

- (id)initWithName:(NSString *)name andHp:(int)hp andMp:(int)mp
{
    @autoreleasepool {
        self = [super init];
        if (self)
        {
            _name = name;
            _hp = hp;
            _mp = mp;
        }
        return self;
    }
}

- (void)useSpell
{
    NSLog(@"Spell used!");
    _useCount++;
}

@end
