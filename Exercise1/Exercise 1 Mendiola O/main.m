//
//  main.m
//  Exercise 1 Mendiola O
//
//  Created by Ojay Mendiola on 5/23/17.
//  Copyright © 2017 Ojay Mendiola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Spell.h"
#import "Wizard.h"

int main(int argc, const char * argv[])
{
    // How to create an instance
    Spell *sampleSpell = [Spell spellWithName:@"Fireball" andCost:100 andDamage:20];
    Wizard *sampleWizard = [Wizard wizardWithName:@"Fireball" andHp:100 andMp:100];
    
    // How to call a function
    [sampleWizard useSpell];
    
    return 0;
}
