#import "MainScene.h"
#import "Asteroid.h"
#import "PlayerShip.h"

@interface MainScene()

@property (nonatomic, readwrite) float timeElapsed;

@end

@implementation MainScene


-(instancetype)init
{
    if((self = [super init])){
        srand(time(NULL)); // Seed random number
        self.spawnInterval = 2.0f;
    }
    
    return self;
}

- (void)update:(CCTime)delta
{
    _timeElapsed += delta;
    if (_timeElapsed >= self.spawnInterval) {
        // Spawn asteroid
        Asteroid* asteroid = [Asteroid asteroidWithImageNamed:@"meteorBrown_big1.png" andSpeed:2.0f andDirection:ccp(1,1)];
        PlayerShip* playerShip = [PlayerShip playerShipWithImageNamed:@"playerShip1_blue.png" andSpeed:2.0f andDirection:ccp(1,1)];
        
        // How to set anchor point
        //[asteroid setAnchorPoint:ccp(0, 0)];
        [self addChild:asteroid];
        [self addChild:playerShip];
        
        // Random position
        CGSize size = [[CCDirector sharedDirector] viewSize];
        int x = rand() % (int)size.width;
        int y = rand() % (int)size.height;
        
        [asteroid setPosition:ccp(x,y)];
        [playerShip setPosition:ccp(x,y)];
        
        _timeElapsed = 0.0f;
    }
}

@end